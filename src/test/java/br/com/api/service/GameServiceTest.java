package br.com.api.service;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.api.BaseTest;
import br.com.api.model.Game;


public class GameServiceTest extends BaseTest {
	
	@Autowired
	private GameService srv;
	
	@Test
	public void testParse(){
		HashMap<String, Game> list = srv.Parse();
		
		Assert.assertNotNull("Falha: Valor null não é esperado.", list);
		Assert.assertTrue("Falha: O retorno não é do tipo esperado.", list instanceof HashMap<?,?>);
		
	}
}
