package br.com.api;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@WebAppConfiguration
public abstract class BaseControllerTest extends BaseTest {

	protected MockMvc mvc;
	
	@Autowired
	protected WebApplicationContext webAppCtx;
	
	@Before
	public void setUp(){
		mvc = MockMvcBuilders.webAppContextSetup(webAppCtx).build();
	}	

}
