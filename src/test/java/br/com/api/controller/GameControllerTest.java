package br.com.api.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import br.com.api.BaseControllerTest;

public class GameControllerTest extends BaseControllerTest {
	@Test
	public void testGetGames() throws Exception{
		
		MvcResult result = this.mvc.perform(get("/Games")
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();
		
		Assert.assertEquals("Falha: Http Request não retornou status 200 OK", 200, status);
		Assert.assertTrue("Falha: Http Request retornou vazio.", content.trim().length() > 0);
		
	}
}
