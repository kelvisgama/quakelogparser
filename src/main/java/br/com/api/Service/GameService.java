package br.com.api.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.springframework.stereotype.Service;

import br.com.api.model.Game;

@Service
public class GameService {

	private List<String> LogLines;
	private LinkedHashMap<String, Game> ListGames;
	private Game g;
	private int GameCount;

	public GameService() {

	}
	
	/**
	 * Método que converte o Log em uma estrutura de Objetos
	 * @return
	 */
	public LinkedHashMap<String, Game> Parse() {

		try {
			LogLines = Files.readAllLines(Paths.get(getClass().getResource("/games.log").toURI()));
			ListGames = new LinkedHashMap<String, Game>();
			GameCount = 0;
			g = null;

			for (String line : LogLines) {
				String content[] = line.substring(7, line.length() - 1).trim().split(":");

				String cmd = content[0];
				// String info = content[1];
				switch (cmd) {
				case "InitGame":
					if (g != null){
						ListGames.put("game_" + GameCount, g);
					}
						

					GameCount += 1;
					g = new Game();
					break;
				case "ShutdownGame":
					if (g != null)
						ListGames.put("game_" + GameCount, g);

					g = null;
					break;
				case "ClientUserinfoChanged":
					String playerName = content[1].split("\\\\")[1];
					g.addPlayer(playerName);
					break;
				case "Kill":
					g.addToTotalKills();
					
					String killer = content[2].split("killed")[0].trim();
					String killed = content[2].split("killed")[1].split("by")[0].trim();
					
					if(killer.equals("<world>")){
						g.remKillFromPlayer(killed);
					}else{
						g.addKillToPlayer(killer);
					}
					
					break;
				default:
					break;
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
			ListGames = null;
		} catch (URISyntaxException e) {
			e.printStackTrace();
			ListGames = null;
		} catch (Exception e) {
			e.printStackTrace();
			ListGames = null;
		}
		
		
		
		return ListGames;

	}

}

