package br.com.api.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class Game {
	private int Total_Kills = 0;
	private List<String> Players = new ArrayList<String>();
	private LinkedHashMap<String, Integer> Kills = new LinkedHashMap<String, Integer>();

	public Game() {
		super();
	}

	public int getTotal_Kills() {
		return Total_Kills;
	}

	public void setTotal_Kills(int total_Kills) {
		Total_Kills = total_Kills;
	}

	public List<String> getPlayers() {
		return Players;
	}

	public void setPlayers(List<String> players) {
		Players = players;
	}

	public LinkedHashMap<String, Integer> getKills() {
		return Kills;
	}

	public void setKills(LinkedHashMap<String, Integer> kills) {
		Kills = kills;
	}

	/**
	 * Incrementa em 1 a contagem de Kill do Game
	 */
	@JsonIgnore
	public void addToTotalKills() {
		this.Total_Kills += 1;
	}

	/**
	 * Adiciona um novo jogador na lista de jogadores caso ele não exista
	 * 
	 * @param PlayerName
	 *            Nome do Jogador
	 */
	@JsonIgnore
	public void addPlayer(String PlayerName) {
		if (!this.Players.contains(PlayerName)) {
			this.Players.add(PlayerName);
			this.Kills.put(PlayerName, 0);
		}
	}

	/**
	 * Incrementa em 1 a contagem de Kill do Jogador
	 * 
	 * @param PlayerName
	 *            Nome do Jogador
	 */
	@JsonIgnore
	public void addKillToPlayer(String PlayerName) {
		this.Kills.put(PlayerName, this.Kills.get(PlayerName) + 1);
	}

	/**
	 * Decrementa em 1 a contagem de Kill do Jogador
	 * 
	 * @param PlayerName
	 *            Nome do Jogador
	 */
	@JsonIgnore
	public void remKillFromPlayer(String PlayerName) {
		this.Kills.put(PlayerName, this.Kills.get(PlayerName) - 1);
	}

}
