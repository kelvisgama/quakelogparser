package br.com.api.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.model.Game;
import br.com.api.service.GameService;

@RestController
public class GameController {

	private GameService service = new GameService();

	@RequestMapping(value = "/Games", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public LinkedHashMap<String,Game> getGames() {
		
		LinkedHashMap<String,Game> listGames = service.Parse(); 
	
		return listGames;
	}
}
