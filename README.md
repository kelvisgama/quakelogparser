# Quake Log Parser API

Este RestFul API desenvolvido utilizando a linguagem **Java** converte o arquivo `games.log` localizado na pasta **resources** deste projeto no formato **JSON** com as informações de total de kills do game, lista de jogadores e total de kills por jogador.

Clone do projeto:
```
#!git

git clone https://kelvisgama@bitbucket.org/kelvisgama/quakelogparser.git
```

### Dependências 

A API foi desenvolvida na IDE Eclipse Neon e foram utilizados alguns frameworks e plugins para facilitar e agilizar o desenvolvimento:

* [Maven](https://maven.apache.org/)
* [Mockito](http://mockito.org/)
* [Spring Boot](http://projects.spring.io/spring-boot/)
* [Spring Tool Suite](https://spring.io/tools/sts/all)
* [Junit](http://junit.org/junit4/)

A versão do eclipse utilizada já possui todas as dependências, com exceção do [Spring Tool Suite](https://spring.io/tools/sts/all) que foi adicionado à IDE para facilitar a criação do projeto.


### Setup da aplicação

##### Executar através do Eclipse

1. Importar projeto para o workspace: `File`->`Import`->`Existing Maven Projects`
2. Executar como `Java Application`

##### Executar como packaged application

`$ java -jar target/QuakeLogParser-0.0.1-SNAPSHOT.jar` 

Após executar algum dos métodos acima, acesse `localhost:8080/Games`

## O Desafio e as Regras

Construir um parser para o arquivo de log `games.log` e expor uma API de consulta.

O arquivo `games.log` é gerado pelo servidor de quake 3 arena. Ele registra todas as informações dos jogos, quando um jogo começa, quando termina, quem matou quem, quem morreu porque caiu no vazio, quem morreu machucado, entre outros.

O desafio consiste em duas tarefas:

1. Construir o parser
2. Expor uma API de consulta que retorne os dados do parser no formato JSON

#### O Log


```
#!log

 20:37 InitGame: ...
 20:38 ClientConnect: 2
 20:38 ClientUserinfoChanged: 2 n\Isgalamido ...
 20:38 ClientBegin: 2
 20:40 Item: 2 weapon_rocketlauncher
 20:40 Item: 2 ammo_rockets
 20:42 Item: 2 item_armor_body
 20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT
 20:59 Item: 2 weapon_rocketlauncher
 21:04 Item: 2 ammo_shells
 21:07 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT

```

#### Saída JSON esperada

```
#!json
"game_1": {
        "total_kills": 45,
        "players": ["Dono da bola", "Isgalamido", "Zeh"],
        "kills": {
          "Dono da bola": 5,
          "Isgalamido": 18,
          "Zeh": 20
        }
      }
```

#### Regras

1. Quando o `<world>` mata o player ele perde -1 kill. *Obs: Decidi adotar essa regra também quando o player mata ele mesmo.*
2. `<world>` não é um player e não deve aparecer na lista de players e nem no dicionário de kills.
3. `total_kills` são os kills dos games, isso inclui mortes do `<world>`.


## Solução

#### Arquitetura

* Criado uma camada `Model` com a classe `Game` que representa um game.
* Foi criado uma camada `Controller` com um Método `GET` que retorna todo o relatório.
* Uma camada `Service` que possui o método `Parse`.
* Camada de testes com duas classes principais, uma para o `service` e outra para o `controller`.

#### Parse

O método parse foi desenvolvido bem simples, nele é utilizando apenas `.Split()` para tratar e identificar os comandos e informações de cada linha do arquivo de log.

#### Teste

Para criação de testes unitários, foram utilizados os recursos do `Junit` e `Mockito` para escrever os `asserts`. O `Mokito` é muito útil para escrever testes de chamada ao método da API sem a necessidade de escrever um `Rest Client`.

O `Maven` por usa vez, garante que a aplicação não poderá ser compilada se algum dos testes falharem, interrompendo o `Build` e gerando uma exceção. 


## Melhorias

A solução foi desenvolvida estritamente nos requisitos propostos, porém deixo aqui algumas melhorias como sugestão:

* Coletar mais dados do arquivo de log, como nome da sala, tempo do jogo, quantidade de Kills por armas (means), etc.;
* Utilizar security (autenticação) no API;
* Criar uma view default para exibir os métodos expostos no API;
* Explorar melhor os demais verbos do Rest, por exemplo: *poderia ser desenvolvido um método POST/PUT que enviaria e atualizaria o arquivo `games.log`.*


## Conclusão

O primeiro item dos requisitos desse desafio é usar a linguagem que tenho mais habilidade, logo pensei em utilizar C#, não que eu tenha preferência pela linguagem porém nos últimos anos é linguagem que mais tenho trabalhado. Assim que iniciei o projeto vi que, de certa forma, já sabia todo o caminho a percorrer para resolver o dado problema e que isso não seria um desafio muito complexo.

Então resolvi fazer em Java, que é uma linguagem que gosto, usei na faculdade e durante 4 anos no desenvolvimento de aplicativos Android. Como eu nunca havia trabalhando com Java em projetos Web MVC e APIs RestFul, tive que reciclar boa parte do meu conhecimento, pesquisar os últimos frameworks disponíveis e aprender a utilizar testes unitários. Foi um desafio muito prazeroso que me rendeu algumas horas noturnas de diversão :)